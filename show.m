
clear, clc, close all;
file = fopen('./build/hough_scene.txt','r');
tid = fscanf(file, '%f');
figure ('name', 'TID on oriented points');
[x,y,z] = sphere (127);
texture = zeros (size(z,1), size(z,2));

theta_space = pi/64;
phi_space = 2*pi/128;

for i = 1: size(z,1)
    for j = 1: size(z,2)
        [azimuth,elevation,r]=cart2sph(x(i,j),y(i,j),z(i,j));
        if azimuth<0;	azimuth=azimuth+2*pi;	end;

		theta_show=pi/2-elevation;
		phi_show=azimuth;
			
		theta_index_show=floor(theta_show/theta_space)+1;
		phi_index_show=floor(phi_show/phi_space)+1;
        
        if theta_index_show > 128
            theta_index_show = theta_index_show - 128;
        end
        
        if phi_index_show > 128
            phi_index_show = phi_index_show - 128;
        end
        
        texture (i,j) = tid((theta_index_show-1)*128+phi_index_show);
    end
 end

globe=surf(x,y,z,'FaceColor','none','EdgeColor',0.5*[1 1 1]);
set(globe,'Facecolor','texturemap','CData',texture,'EdgeColor','none');
xlabel('X',  'FontSize', 20, 'FontWeight', 'bold');
ylabel('Y',  'FontSize', 20, 'FontWeight', 'bold');
zlabel('Z',  'FontSize', 20, 'FontWeight', 'bold');
axis equal
hold off

figure ('name', 'TID on oriented points (Image)');
Tid_image = reshape(tid, [128,128]);
Tid_image = Tid_image';
imshow(Tid_image, [])
xlabel('Azimuth',  'FontSize', 20, 'FontWeight', 'bold');
ylabel('Elevation','FontSize', 20, 'FontWeight', 'bold');
hold on
M = size(Tid_image,1);
N = size(Tid_image,2);

for k = 1:8:M
    x = [1 N];
    y = [k k];
    plot(x,y,'Color','[0.2 0.2 0.2]','LineStyle','-');
    plot(x,y,'Color','[0.2 0.2 0.2]','LineStyle',':');
end

for k = 1:8:N
    x = [k k];
    y = [1 M];
    plot(x,y,'Color','[0.2 0.2 0.2]','LineStyle','-');
    plot(x,y,'Color','[0.2 0.2 0.2]','LineStyle',':');
end

axis equal;

hold off
