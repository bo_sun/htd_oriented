/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <cmath>
#include <algorithm>
#include <vector>
#include <eigen3/Eigen/Dense>

// For debug
#include <time.h>
#include <fstream>

#include "tams_htd_oriented.h"
#include "tams_htd_oriented.hpp"

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
// Types
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointCloudNT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/

std::string object_filename_;
std::string scene_filename_;
Eigen::Vector2i hough_size;

void showhelp(char *filename)
{
    std::cout << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "-                     TAMS_HTD_Oriented                                -" << std::endl;
    std::cout << "-                                                                      -" << std::endl;
    std::cout << "------------------------------------------------------------------------" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << filename << "object_filename.pcd scene_filename.pcd [Options]" << std::endl;
    std::cout << std::endl;
    std::cout << "Options:" << std::endl;
    std::cout << "      -h               Show this help." << std::endl;
    std::cout << "      --hough_size     Set the dimension of Hough Transform defined on sphere" << std::endl;
    std::cout << std::endl;
}

void parseCommandLine(int argc, char **argv)
{
    if (argc < 3)
    {
        showhelp(argv[0]);
        exit(-1);
    }

    if (pcl::console::find_switch(argc, argv, "-h"))
    {
        showhelp(argv[0]);
        exit(0);
    }

    std::vector<int> filenames;
    filenames = pcl::console::parse_file_extension_argument(argc, argv, ".pcd");
    if (filenames.size()!=2)
    {
        std::cout << "\nFilenames missing.\n";
        showhelp(argv[0]);
        exit(-1);
    }
    object_filename_ = argv[filenames[0]];
    scene_filename_ = argv[filenames[1]];

    if(pcl::console::parse_2x_arguments(
                argc, argv, "--hough_size", hough_size(0), hough_size(1))
            ==-1)
    {
        hough_size << 128, 128;
    }
}

int
main (int argc, char**argv)
{
    parseCommandLine(argc, argv);

    // Point clouds
    PointCloudNT::Ptr object(new PointCloudNT);
    PointCloudNT::Ptr scene(new PointCloudNT);

    // Load object and scene point clouds
    pcl::console::print_highlight("Loading point clouds...\n");
    pcl::console::print_info(">>>Ignore the below warning please<<<\n");
    pcl::console::print_info("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
    if (pcl::io::loadPCDFile<PointNT>(object_filename_, *object)<0 ||
            pcl::io::loadPCDFile<PointNT>(scene_filename_,*scene)<0)
    {
        pcl::console::print_error("Error loading object/scene file!\n");
        return (1);
    }
    pcl::console::print_info("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n");
    pcl::console::print_info(">>>Ignore the above warning please<<<\n");

    // Remove the NaN points in object and scene if any
    pcl::console::print_highlight("Remove the NaN points if any...\n");
    std::vector<int> indices_object_nan, indices_scene_nan;
    pcl::removeNaNFromPointCloud(*object,*object,indices_object_nan);
    pcl::removeNaNFromPointCloud(*scene, *scene, indices_scene_nan);

    // Estimate normal of point clouds
    pcl::console::print_highlight("Estimate normal of points...\n");
    pcl::NormalEstimation<PointNT, PointNT> norm_est;
    pcl::search::KdTree<PointNT>::Ptr tree(new pcl::search::KdTree<PointNT>());
    norm_est.setSearchMethod(tree);
    norm_est.setKSearch(30);
//  norm_est.setRadiusSearch(0.25);
    norm_est.setInputCloud(object);
    norm_est.compute(*object);
    norm_est.setInputCloud(scene);
    norm_est.compute(*scene);

    // Compute Hough Transform based on the oriented points
    pcl::console::print_highlight("Compute Hough Transform based on oriented points...\n");
    EigenMatrixRowXf  hough_object = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));
    EigenMatrixRowXf  hough_scene  = EigenMatrixRowXf::Zero(hough_size(0), hough_size(1));
    computeHough(*object, hough_size(0), hough_size(1),
                 hough_object);
    computeHough(*scene,  hough_size(0), hough_size(1),
                 hough_scene);

    // store the Hough Transform into a text file
    // hough_object is row major matrix
    ofstream file;
    file.open("hough_object.txt");
    for (int i = 0; i<hough_object.size(); i++)
    {
        file << *(hough_object.data()+i) << std::endl;
    }
    file.close();

    ofstream file1;
    file1.open("hough_scene.txt");
    for (int i = 0; i<hough_scene.size(); i++)
    {
        file1 << *(hough_scene.data()+i) << std::endl;
    }
    file1.close();
    return (0);
}

