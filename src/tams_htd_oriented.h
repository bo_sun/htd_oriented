/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/

#ifndef TAMS_HTD_ORIENTED_H_
#define TAMS_HTD_ORIENTED_H_

#ifndef TYPE_DEFINITION_
#define TYPE_DEFINITION_
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/pcl_visualizer.h>
// Types
typedef pcl::PointNormal PointNT;
typedef pcl::PointCloud<PointNT> PointCloudNT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointNT> ColorHandlerT;
typedef Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> EigenMatrixRowXf;
#endif /*TYPE_DEFINITION_*/


/** \brief cart2sph(X,Y,Z, azimuth, polar) transforms Cartesian coordinates stored in
  * corresponding elements of arrays X, Y, and Z into spherical coordinates.
  * azimuth and polar are angular displacements in radians.
  * azimuth(longitudinal) is the counterclockwise angle in the x-y plane
  * measured from the positive x-axis.
  * polar(colatitudianl) is the polar angle measured from the z axis.
  *
  * 0 < azimuth < 2*M_PI; 0 < polar < M_PI
  */
void tams_cart2sph(float x, float y, float z,
                   float& azimuth, float& polar);

/** \brief computeHough calculate the Hough transform
  * of oriented points, and the Hough transform
  * is defined on a Gaussian sphere.
  * Parameters:
  * [in]  cloud               the input point cloud
  * [in]  hough_polar_bin     the number of bins for polar
  * [in]  hough_azimuth_bin   the number of bins for azimuth
  * [out] hough               the resultant hough transform stored in a
  *                           hough_polar_bin X hough_azimuth_bin matrix
  */
void computeHough (const PointCloudNT cloud,
                   size_t hough_polar_bin,
                   size_t hough_azimuth_bin,
                   EigenMatrixRowXf &hough);

/** \brief PhaseCorrelation2D compute the offset between two input images
  * based on POMF (Phase Only Matched Filter)
  * -->> Q(k) = conjugate(S(k))/|S(k)| * R(k)/|R(k)|
  * -->> q(x) = ifft(Q(k))
  * -->> (xs,ys) = argmax(q(x))
  * Note that the storage order of FFTW is row-order, while the storage
  * order of Eigen is default column-order.
  * Parameters:
  * [in] signal              the input(signal) image
  * [in] pattern             the input(pattern) image
  * [in] height              the height of input images(how many rows/size of column/range of x)
  * [in] width               the width of input images (how many columns/size of row/range of y)
  * [out] height_offset      the result offset, we move down (positive x axis) pattern height_offset to match signal
  * [out] width_offset       the result offset, we move right (positive y axis) pattern width_offset to match signal
  */
void PhaseCorrelation2D(const EigenMatrixRowXf signal,
                        const EigenMatrixRowXf pattern,
                        const int height,
                        const int width,
                        int &height_offset,
                        int &width_offset);

/** \brief PhaseCorrelation3D compute the offset between two input volumes
  * based on POMF (Phase Only Matched Filter)
  * -->> Q(k) = conjugate(S(k))/|S(k)| * R(k)/|R(k)|
  * -->> q(x) = ifft(Q(k))
  * -->> (xs,ys) = argmax(q(x))
  * Note that the storage order of FFTW is row-order
  * We adopt the RIGHT-hand Cartesian coordinate system.
  * Parameters:
  * [in] signal              the input(signal) volume
  * [in] pattern             the input(pattern) volume
  * [in] height              the height of input volumes(range of x)
  * [in] width               the width of input volumes (range of y)
  * [in] depth               the depth of input volumes (range of z)
  * [out] height_offset      the result offset, we move down (positive x axis) pattern height_offset to match signal
  * [out] width_offset       the result offset, we move right (positive y axis) pattern width_offset to match signal
  * [out] depth_offset       the result offset, we move close to viewer (positive z axis) pattern depth_offset to match signal
  */
void PhaseCorrelation3D(const double *signal,
                        const double *pattern,
                        const int height,
                        const int width,
                        const int depth,
                        int &height_offset,
                        int &width_offset,
                        int &depth_offset);

/** \brief voxelsize2volumesize compute the size of volume
  * the points cloud rendered based on the size of voxel.
  * Parameters:
  * [in]  cloud         the input point cloud
  * [in]  voxelsize     the size of the voxel
  * [out] volumesize    the size of volume the input cloud should be rendered to
  */
void voxelsize2volumesize ( const PointCloudNT cloud,
                            Eigen::Vector3f voxelsize,
                            Eigen::Vector3i &volumesize);

/** \brief point2volume render the input point cloud
  * to a volume, assign the number of points (or max curvature of points)
  * in a voxel to the value of voxel.
  * Parameters:
  * [in]  cloud              the input point cloud
  * [in]  voxelsize          the size of the voxel
  * [in]  volumesize         the real size of resutant volume
  * [in]  volumesize_origin  the volumesize calculated by function "voxelsize2volumesize"
  * [out] volume             the resultant volume contained in the
  *                          volumesize(0)*volumesize(1)*volumesize(2) matrix
  * [in]  Usecurvature       whether to assign the max curvature of points in a voxel to the value of voxel.
  */
void point2volume (const PointCloudNT cloud,
                   Eigen::Vector3f voxelsize,
                   Eigen::Vector3i volumesize,
                   Eigen::Vector3i volumesize_origin,
                   double *volume,
                   bool Usecurvature = false);

#endif /*TAMS_HOUGH_REGISTRATION_H_*/
