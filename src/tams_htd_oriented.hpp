/*********************************************
 * Author: Bo Sun                            *
 * Afflication: TAMS, University of Hamburg  *
 * E-Mail: bosun@informatik.uni-hamburg.de   *
 *         user_mail@QQ.com                  *
 * Date: Nov 13, 2014                        *
 * Licensing: GNU GPL license.               *
 *********************************************/
#ifndef TAMS_HTD_ORIENTED_HPP_
#define TAMS_HTD_ORIENTED_HPP_

#include "tams_htd_oriented.h"

#include <cmath>
#include <algorithm>
#include <eigen3/Eigen/Dense>

#include "fftw3.h"

void tams_cart2sph(float x, float y, float z,
                   float& azimuth, float& polar)
{
    polar = atan2(hypot(x,y),z);
    azimuth = atan2(y,x);
    if (azimuth < 0)
        azimuth = azimuth + 2*M_PI;
}

void computeHough (const PointCloudNT cloud,
                   size_t hough_polar_bin,
                   size_t hough_azimuth_bin,
                   EigenMatrixRowXf &hough)
{
    PointNT minpt, maxpt;
    pcl::getMinMax3D<PointNT> (cloud, minpt, maxpt);
    float azimuth_spa = 2*M_PI/hough_azimuth_bin;
    float polar_spa = M_PI/hough_polar_bin;

    float temp_azi, temp_polar, temp_rho;
    size_t temp_azith, temp_polorth;

    for (PointCloudNT::const_iterator itr=cloud.begin();
         itr!=cloud.end();itr++)
    {
        if(((*itr).z - minpt.z)<0.1*(maxpt.z-minpt.z))
            continue;

        tams_cart2sph((*itr).normal_x, (*itr).normal_y, (*itr).normal_z,
                      temp_azi, temp_polar);
        temp_azith = floor(temp_azi/azimuth_spa);
        temp_polorth = floor(temp_polar/polar_spa);

        temp_rho = sqrt((*itr).x *(*itr).x + (*itr).y *(*itr).y + (*itr).z *(*itr).z);
        hough(temp_polorth,temp_azith) = hough(temp_polorth,temp_azith) + temp_rho;
    }
}

void PhaseCorrelation2D(const EigenMatrixRowXf signal,
                        const EigenMatrixRowXf pattern,
                        const int height,
                        const int width,
                        int &height_offset,
                        int &width_offset)
{
    // load data
    if (signal.size() != width*height ||
            pattern.size() !=width*height)
    {
        std::cout << "The size of image input for PhaseCorrelation wrong!" << std::endl;
        return ;
    }

    fftw_complex *signal_img = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*height*width);
    fftw_complex *pattern_img = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*height*width);

    for (int i=0; i < signal.size(); i++)
    {
        signal_img[i][0] = *(signal.data()+i);
        signal_img[i][1] = 0;
    }
    for (int j=0; j < pattern.size(); j++)
    {
        pattern_img[j][0] = *(pattern.data()+j);
        pattern_img[j][1] = 0;
    }

    // forward fft
    fftw_plan signal_forward_plan = fftw_plan_dft_2d (height, width, signal_img, signal_img,
                                                    FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pattern_forward_plan  = fftw_plan_dft_2d (height, width, pattern_img, pattern_img,
                                                    FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute (signal_forward_plan);
    fftw_execute (pattern_forward_plan);

    // cross power spectrum
    fftw_complex *cross_img = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*height*width);
    double temp;
    for (int i=0; i < height*width; i++)
    {
        cross_img[i][0] = (signal_img[i][0]*pattern_img[i][0])-
                (signal_img[i][1]*(-pattern_img[i][1]));
        cross_img[i][1] = (signal_img[i][0]*(-pattern_img[i][1]))+
                (signal_img[i][1]*pattern_img[i][0]);
        temp = sqrt(cross_img[i][0]*cross_img[i][0]+cross_img[i][1]*cross_img[i][1]);
        cross_img[i][0] /= temp;
        cross_img[i][1] /= temp;
    }

    // backward fft
    // FFTW computes an unnormalized transform,
    // in that there is no coefficient in front of
    // the summation in the DFT.
    // In other words, applying the forward and then
    // the backward transform will multiply the input by n.

    // BUT, we only care about the maximum of the inverse DFT,
    // so we don't need to normalize the inverse result.

    // the storage order in FFTW is row-order
    fftw_plan cross_backward_plan = fftw_plan_dft_2d(height, width, cross_img, cross_img,
                                                     FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(cross_backward_plan);

    // free memory
    fftw_destroy_plan(signal_forward_plan);
    fftw_destroy_plan(pattern_forward_plan);
    fftw_destroy_plan(cross_backward_plan);
    fftw_free(signal_img);
    fftw_free(pattern_img);

    Eigen::VectorXf cross_real = Eigen::VectorXf::Zero(height*width);
    for (int i= 0; i < height*width; i++)
    {
        cross_real(i) = cross_img[i][0];
    }

    std::ptrdiff_t max_loc;
    float unuse = cross_real.maxCoeff(&max_loc);

    height_offset =floor(((int) max_loc)/ width);
    width_offset = (int)max_loc - width*height_offset;

    if (height_offset > 0.5*height)
        height_offset = height_offset-height;
    if (width_offset  > 0.5*width)
        width_offset = width_offset-width;
}

void PhaseCorrelation3D(const double *signal,
                        const double *pattern,
                        const int height,
                        const int width,
                        const int depth,
                        int &height_offset,
                        int &width_offset,
                        int &depth_offset)
{
    int size = height*width*depth;
    fftw_complex *signal_volume  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);
    fftw_complex *pattern_volume = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);

    for (int i=0; i < size; i++)
    {
        signal_volume[i][0] = signal[i];
        signal_volume[i][1] = 0;
    }
    for (int j=0; j < size; j++)
    {
        pattern_volume[j][0] = pattern[j];
        pattern_volume[j][1] = 0;
    }

    // forward fft
    fftw_plan signal_forward_plan = fftw_plan_dft_3d (height, width, depth, signal_volume, signal_volume,
                                                    FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pattern_forward_plan  = fftw_plan_dft_3d (height, width, depth, pattern_volume, pattern_volume,
                                                    FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute (signal_forward_plan);
    fftw_execute (pattern_forward_plan);

    // cross power spectrum
    fftw_complex *cross_volume = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*size);
    double temp;
    for (int i=0; i < size; i++)
    {
        cross_volume[i][0] = (signal_volume[i][0]*pattern_volume[i][0])-
                (signal_volume[i][1]*(-pattern_volume[i][1]));
        cross_volume[i][1] = (signal_volume[i][0]*(-pattern_volume[i][1]))+
                (signal_volume[i][1]*pattern_volume[i][0]);
        temp = sqrt(cross_volume[i][0]*cross_volume[i][0]+cross_volume[i][1]*cross_volume[i][1]);
        cross_volume[i][0] /= temp;
        cross_volume[i][1] /= temp;
    }

    // backward fft
    // FFTW computes an unnormalized transform,
    // in that there is no coefficient in front of
    // the summation in the DFT.
    // In other words, applying the forward and then
    // the backward transform will multiply the input by n.

    // BUT, we only care about the maximum of the inverse DFT,
    // so we don't need to normalize the inverse result.

    // the storage order in FFTW is row-order
    fftw_plan cross_backward_plan = fftw_plan_dft_3d(height, width, depth, cross_volume, cross_volume,
                                                     FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(cross_backward_plan);

    // free memory
    fftw_destroy_plan(signal_forward_plan);
    fftw_destroy_plan(pattern_forward_plan);
    fftw_destroy_plan(cross_backward_plan);
    fftw_free(signal_volume);
    fftw_free(pattern_volume);

    Eigen::VectorXf cross_real(size);

    for (int i= 0; i < size; i++)
    {
        cross_real(i) = cross_volume[i][0];
    }

    std::ptrdiff_t max_loc;
    float unuse = cross_real.maxCoeff(&max_loc);

    height_offset =floor(((int) max_loc)/ (width*depth));
    width_offset = floor(((int)max_loc - width*depth*height_offset)/depth);
    depth_offset = floor((int)max_loc-width*depth*height_offset-width_offset*depth);

    if (height_offset > 0.5*height)
        height_offset = height_offset-height;
    if (width_offset  > 0.5*width)
        width_offset = width_offset-width;
    if (depth_offset > 0.5*depth)
        depth_offset = depth_offset-depth;
}


void voxelsize2volumesize ( const PointCloudNT cloud,
                            Eigen::Vector3f voxelsize,
                            Eigen::Vector3i &volumesize)
{
    // Note that only the x,y,z field of
    // 'minpt&maxpt' make sense
    // That is downside of template, I guess
    PointNT minpt, maxpt;
    pcl::getMinMax3D<PointNT> (cloud, minpt, maxpt);

    volumesize(0) = ceil((maxpt.x-minpt.x)/voxelsize(0))+1;
    volumesize(1) = ceil((maxpt.y-minpt.y)/voxelsize(1))+1;
    volumesize(2) = ceil((maxpt.z-minpt.z)/voxelsize(2))+1;

    // pad the volume to be of 2^n, which FFTW favors
    if (remainder(volumesize(0),2)!=0)
        volumesize(0)++;
    if (remainder(volumesize(1),2)!=0)
        volumesize(1)++;
    if (remainder(volumesize(2),2)!=0)
        volumesize(2)++;
}

void point2volume (const PointCloudNT cloud,
                   Eigen::Vector3f voxelsize,
                   Eigen::Vector3i volumesize,
                   Eigen::Vector3i volumesize_origin,
                   double *volume,
                   bool Usecurvature)
{
    int x_index, y_index, z_index;
    PointNT origin_minpt, origin_maxpt;
    pcl::getMinMax3D<PointNT> (cloud, origin_minpt, origin_maxpt);

    // determine the range of volume
    PointNT minpt, maxpt;
    // please note that the elements of volumesize are all even,
    // so is the volumesize distance
    Eigen::Vector3i volumesize_dist = volumesize_origin - volumesize;
    minpt.x = origin_minpt.x + volumesize_dist(0)/2*voxelsize(0);
    maxpt.x = origin_maxpt.x - volumesize_dist(0)/2*voxelsize(0);
    minpt.y = origin_minpt.y + volumesize_dist(1)/2*voxelsize(1);
    maxpt.y = origin_maxpt.y - volumesize_dist(1)/2*voxelsize(1);
    minpt.z = origin_minpt.z + volumesize_dist(2)/2*voxelsize(2);
    maxpt.z = origin_maxpt.z - volumesize_dist(2)/2*voxelsize(2);

    // core part of generate volume
    for (PointCloudNT::const_iterator itr=cloud.begin();
         itr!=cloud.end(); itr++)
    {
        if (    (*itr).x > minpt.x && (*itr).x < maxpt.x &&
                (*itr).y > minpt.y && (*itr).y < maxpt.y &&
                (*itr).z > minpt.z && (*itr).z < maxpt.z)
        {
            x_index = floor(((*itr).x - minpt.x)/voxelsize(0));
            y_index = floor(((*itr).y - minpt.y)/voxelsize(1));
            z_index = floor(((*itr).z - minpt.z)/voxelsize(2));

            if (Usecurvature == true)
            {
                if (isFinite(*itr) &&
                        (*itr).curvature > volume[z_index+volumesize(2)*(y_index+volumesize(1)*x_index)])
                    volume[z_index+volumesize(2)*(y_index+volumesize(1)*x_index)] = (*itr).curvature;
            }
            else
            {
                if(isFinite(*itr))
                    volume[z_index+volumesize(2)*(y_index+volumesize(1)*x_index)]++;
            }
        }
    }
}
#endif /*TAMS_HOUGH_REGISTRATION_HPP_*/
